# bash commands

## which shell

```bash
which $SHELL
```

## bash script

always begin with #!/usr/bin/bash <- this is location of running program
variable use UPPERCASE name and equal and use it with $ like below

```bash
#!/usr/bin/bash
echo "my first bash script"

GREET="Howdy partner"
echo $GREET
```

### bash conditional logic

```bash
#!/usr/bin/bash
if [$2 -lt 21]; then
  echo "You are too young to use this script"
else
  echo "Enjoy your beer $1!"
fi
```

### bash script arguments

```bash
#!/usr/bin/bash
echo "Name: $1";
echo "Age: $2";
```

### bash script input in middle of program

```bash
#!/usr/bin/bash
while true; do
  read -p "Do you wish to drink a beer?" yn
  case $yn in
    [Yy]* ) break;;
    [Nn]* ) exit;;
    *) echo "Please answer yes or no. ";;
  esac
done
```
