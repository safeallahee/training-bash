# Linux

## General commands

pwd -> print working directory
cp -> copy file
rm -> remove file
rm -i -> ask to delete a file
mv -> move file
mv old_name new_name -> rename file

## Filesystem Layout

bin -> program binary lives here
boot -> boot file lives (kernel including configuration files)
cdrom -> (what we call mount point) for mounting cdrom to filesystem
dev -> all of devices are here (in linux all devices are files)
etc -> configuration files for different system, programs, and utilities
home -> home directories
lib -> libraries of programs (like dll in windows)
lib64 -> libraries 64
media -> for usb stick mounted*

- psf (post operating system) file inside is parallels

mnt -> another place to mount removable media*
opt -> optional software
proc -> processor related data (sudo entry it doesn't really exists)

- every process get owns directory here
- every process id have its own directory

root -> root home directory
run -> get some temporary data gets to work*
sbin -> system binaries or system programs
srv -> for particular server that puts data here
sys -> system data (another sudo file system)
tmp -> tmp where swap file are store
usr -> user related data is store here

- user accessible binaries

var -> any variable data

- log here store all system logs

## File permissions

owner group world
1 -> executable
2 -> write
4 -> read

chmod 777 file.name

u+x -> user executable
g+x -> group executable
w+x -> world executable
g-x -> remove use executable
chmod g+x file.name

## Environment variable

$PATH is variable that indicate where the system should look
for programs and executables.
for show $PATH use echo $PATH.
directories separated by the colon(:).
for add path to $PATH variable use:

```bash
export PATH=$PATH:~/bin
```

## Editing file

simple way to create file and put content in it is:

```bash
echo this is file > file
```

cat is program that concatenate files and if you cat just one file it
will show file content in console
